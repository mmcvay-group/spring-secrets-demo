### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).


### Custom Secrets Detection

A _gitleaks.toml_ file includes an example of a custom rule for secrets detection. It will detect secrets in the _application.properties_ file in _src/main/resources_.  A validate job has been included to fail the pipeline if any secrets are detected, however, you may not wish to do this in the real world.
